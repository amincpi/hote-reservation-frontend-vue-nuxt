import { createStore } from "vuex";
import UserMoudle from './vuex-modules/user';
import CustomerMoudle from './vuex-modules/customer';
const store = createStore({
    modules: {
      user: UserMoudle,
      customer: CustomerMoudle
    }
  })
export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.use(store);
  // Install the store instance as a plugin
});