importScripts('https://www.gstatic.com/firebasejs/8.2.7/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.7/firebase-messaging.js');
// Initialize the Firebase app in the service worker by passing the generated config
var firebaseConfig = {
    apiKey: "AIzaSyDBVCVV0pS2hZzhlmRA0UiZBsCTY2d0c7A",
    authDomain: "hotel-booking-61c6b.firebaseapp.com",
    projectId: "hotel-booking-61c6b",
    storageBucket: "hotel-booking-61c6b.appspot.com",
    messagingSenderId: "795794744522",
    appId: "1:795794744522:web:0cd22439bb35efcfef0033",
    measurementId: "G-5T9NCWBW3V"
};
firebase.initializeApp(firebaseConfig);
// Retrieve firebase messaging
const messaging = firebase.messaging();
messaging.onBackgroundMessage(function (payload) {
  console.log('Received background message ', payload);
  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body
  };
  self.registration.showNotification(notificationTitle,
    notificationOptions);
});