// https://nuxt.com/docs/api/configuration/nuxt-config
const fs = require('fs')
export default defineNuxtConfig({
  css: ['ant-design-vue/dist/antd.css'],
  plugins: [
    {src: '~/plugins/antd.ts', mode: 'client'},
    {src: '~/plugins/vuex.ts'}
  ],
  modules: [
    '@nuxtjs/firebase',    
  ],
  // modules: [
  //   [
  //     '@nuxtjs/firebase',
  //     {
  //       config: {
  //         apiKey: "AIzaSyDBVCVV0pS2hZzhlmRA0UiZBsCTY2d0c7A",
  //         authDomain: "hotel-booking-61c6b.firebaseapp.com",
  //         projectId: "hotel-booking-61c6b",
  //         storageBucket: "hotel-booking-61c6b.appspot.com",
  //         messagingSenderId: "795794744522",
  //         appId: "1:795794744522:web:0cd22439bb35efcfef0033",
  //         measurementId: "G-5T9NCWBW3V"
  //       },
  //       onFirebaseHosting: false,
  //       services: {
  //         messaging: {
  //           createServiceWorker: true,
  //           fcmPublicVapidKey: 'BEFjuJp0aVcmMz8_GXuEJi6Jgb4P2D5Tcmp0VRBtv7_4MGfpCTlV2F0e7eKkyuQvqkuyRi3AnSaeQOVH_wyAX6M',
  //           inject: fs.readFileSync('./serviceWorker.js')
  //         }
  //       }
  //     }
  //   ]
  // ],
})
